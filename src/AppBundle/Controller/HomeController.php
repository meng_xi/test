<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;

class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function homeAction(Request $request)
    {
        $id = $this->getParameter('default_id');
        $counter = $this->getDoctrine()->getRepository('AppBundle:Counter')->find($id);
        if(!$counter) {
            throw $this->createNotFoundException();
        }
        
        return $this->render('home/home.html.twig', array(
            'count' => $counter->getCount(),
        ));
    }
    
    /**
     * @Method({"POST"})
     * @Route("/increase", name="increase")
     */
    public function increaseAction(Request $request)
    {
        if (!$this->isCsrfTokenValid('increase_token', $request->request->get('_token'))) {
            return new JsonResponse(['success' => false, 'message' => 'Token invalid.']);
        }
        
        $id = $this->getParameter('default_id');
        $counter = $this->getDoctrine()->getRepository('AppBundle:Counter')->find($id);
        $counter->setCount($counter->getCount() + 1);
        
        try {
            $this->getDoctrine()->getManager()->flush();
            return new JsonResponse(['success'=> true, 'count' => $counter->getCount()]);
        } catch (\Exception $ex) {
            return new JsonResponse(['success' => false, 'message' => 'Flush error.']);
        }       
    } 
}
