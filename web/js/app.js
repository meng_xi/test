(function($) {
    
    $().ready(function() {
        $('#counter').each(function() {
            var $this = $(this);
            $('#increase-btn').on('click', function(e){
                $.ajax({
                    type: 'POST',
                    url: $this.data('increase-url'),
                    data: {
                        _token: $this.data('increase-token'),
                    },
                    success: function(data){
                        if(data.success) {
                            $this.find('#count').text(data.count);
                        } else {
                            alert(data.message);
                        }
                    }
                });
            });
        });
    });
    
})(window.jQuery);